use glyph_brush::ab_glyph::FontArc;
use glyph_brush::GlyphBrushBuilder;
use glyph_brush::ab_glyph::Font;
use glyph_brush::GlyphCruncher;
use glyph_brush::BrushAction;
use glyph_brush::BrushError;
use glyph_brush::GlyphBrush;
use glyph_brush::Section;
use glyph_brush::Text;
use crate::UsedLength;

#[derive(Default)]
pub struct LabelAttrs<'a> {
	pub txt: &'a str,
	pub grow: usize,
	pub hover: &'a str,
	pub _box: usize,
	pub pad: usize,
	pub style: &'a str,
}

#[derive(Default)]
pub struct TextAttrs<'a> {
	pub txt: &'a str,
	pub dst: &'a str,
	pub grow: usize,
	pub hover: &'a str,
	pub _box: usize,
	pub pad: usize,
	pub style: &'a str,
}

type SixUsize = (usize, usize, usize, usize, usize, usize);

#[derive(Debug)]
pub struct TextContext {
	brush: GlyphBrush<SixUsize>,
	fontarcs: Vec<FontArc>,
	texture: Vec<u8>,
	texture_width: usize,
	texture_height: usize,
	vertices: Vec<SixUsize>,
}

extern "C" {
	fn set_px(x: usize, y: usize, v: usize);
}

impl TextContext {
	pub fn new() -> Self {
		let dejavu = FontArc::try_from_slice(include_bytes!("../font1.ttf")).unwrap();
		let brush = GlyphBrushBuilder::using_font(dejavu)
			.draw_cache_scale_tolerance(0.0)
			.build();
		let fontarcs = brush
			.fonts().iter()
			.map(|font| font.clone())
			.collect::<Vec<FontArc>>();
		let mut ctx = TextContext {
			brush,
			fontarcs,
			texture: Vec::new(),
			texture_width: 0,
			texture_height: 0,
			vertices: Vec::new(),
		};
		ctx.realloc_texture();
		ctx
	}
	
	fn realloc_texture(&mut self) {
		let (w, h) = self.brush.texture_dimensions();
		let tex_len = (w * h) as usize;
		self.texture_width = w as usize;
		self.texture_height = h as usize;
		self.texture = Vec::with_capacity(tex_len);
		self.texture.resize(tex_len, 0);
	}
	
	pub fn process_queue(&mut self) {
		loop {
			let (w, h) = (self.texture_width, self.texture_height);
			let mut to_be_updated = Vec::new();
			let r = self.brush.process_queued(
				|rect, tex_data| {
					let mut data = Vec::with_capacity(tex_data.len());
					data.extend_from_slice(tex_data);
					to_be_updated.push((rect, data));
				}, |vdat| {
					let ox = vdat.extra.color[0] as usize;
					let oy = vdat.extra.color[1] as usize;
					let ww = vdat.pixel_coords.max.x - vdat.pixel_coords.min.x;
					let wh = vdat.pixel_coords.max.y - vdat.pixel_coords.min.y;
					(
						(vdat.tex_coords.min.x * (w as f32)).round() as usize,
						(vdat.tex_coords.min.y * (h as f32)).round() as usize,
						vdat.pixel_coords.min.x as usize - ox,
						vdat.pixel_coords.min.y as usize - oy,
						ww as usize,
						wh as usize,
					)
				}
			);
			match r {
				Ok(action) => {
					for (rect, data) in to_be_updated {
						let len = rect.width() as usize;
						let mut i = (rect.min[0] as usize) + w * (rect.min[1] as usize);
						for chunk in data.chunks(len) {
							self.texture[i..i+len].copy_from_slice(chunk);
							i += w as usize;
						}
					}
					if let BrushAction::Draw(vertices) = action {
						self.vertices = vertices;
					}
					for (tx, ty, ox, oy, ww, wh) in &self.vertices {
						for y in 0..*wh {
							for x in 0..*ww {
								let tv = self.texture[(tx + x) + w * (ty + y)] as usize;
								unsafe { set_px(ox + x, oy + y, tv); }
							}
						}
					}
					break;
				},
				Err(BrushError::TextureTooSmall { suggested }) => {
					self.brush.resize_texture(suggested.0, suggested.1);
					self.realloc_texture();
				}
			}
		}
	}
	
	fn section<'a>(&self, txt: &'a str, font_size: f64) -> Section<'a> {
		let text = Text::new(txt).with_scale(font_size as f32);
		Section::default().add_text(text)
	}
	
	fn bounds(&mut self, section: &Section) -> (f64, f64, f64, f64) {
		let mut x_min = f32::MAX;
		let mut x_max = f32::MIN;
		let mut y_min = f32::MAX;
		let mut y_max = f32::MIN;
		for glyph in self.brush.glyphs(section) {
			let font = &self.fontarcs[glyph.font_id.0];
			if let Some(outlined) = font.outline_glyph(glyph.glyph.clone()) {
				let bounds = outlined.px_bounds();
				if bounds.min.x < x_min {
					x_min = bounds.min.x;
				}
				if bounds.max.x > x_max {
					x_max = bounds.max.x;
				}
				if bounds.min.y < y_min {
					y_min = bounds.min.y;
				}
				if bounds.max.y > y_max {
					y_max = bounds.max.y;
				}
			}
		}
		(
			x_min as f64,
			y_min as f64,
			x_max as f64,
			y_max as f64
		)
	}

	fn aspect_ratio(&mut self, bounds: (f64, f64, f64, f64)) -> Option<f64> {
		let (x_min, y_min, x_max, y_max) = bounds;
		if x_min >= x_max || y_min >= y_max {
			None
		} else {
			let width = x_max - x_min;
			let height = y_max - y_min;
			Some(width / height)
		}
	}
	
	pub fn label_h(&mut self, attrs: LabelAttrs, cross: usize) -> UsedLength {
		let section = self.section(attrs.txt, 100.0);
		let bounds = self.bounds(&section);
		let ratio = self.aspect_ratio(bounds);
		let cross = cross - (attrs.pad * 2);
		let px = match ratio {
			Some(ratio) => ((cross as f64) * ratio) as usize,
			_ => 0,
		} + (attrs.pad * 2);
		UsedLength {
			inner: None,
			grow: attrs.grow,
			px,
		}
	}
	
	pub fn label_v(&mut self, attrs: LabelAttrs, cross: usize) -> UsedLength {
		let section = self.section(attrs.txt, 100.0);
		let bounds = self.bounds(&section);
		let ratio = self.aspect_ratio(bounds);
		let cross = cross - (attrs.pad * 2);
		let px = match ratio {
			Some(ratio) => ((cross as f64) / ratio) as usize,
			_ => 0,
		} + (attrs.pad * 2);
		UsedLength {
			inner: None,
			grow: attrs.grow,
			px,
		}
	}
	
	pub fn label_queue(&mut self, attrs: LabelAttrs, x: usize, y: usize, _w: usize, h: usize) {
		let h_pad = h - 2 * attrs.pad;
		let x_pad = x + attrs.pad;
		let y_pad = y + attrs.pad;
		let font_size = {
			let section = self.section(attrs.txt, 100.0);
			let (_, y_min, _, y_max) = self.bounds(&section);
			let factor = (h_pad as f64) / (y_max - y_min);
			factor * 100.0
		};
		let position = (x_pad as f32, y_pad as f32);
		let mut section = self
			.section(attrs.txt, font_size);
		let (a, b, _, _) = self.bounds(&section);
		section.text[0].extra.color[0] = a as f32;
		section.text[0].extra.color[1] = b as f32;
		self.brush.queue(section.with_screen_position(position));
		crate::element_render(0, x, y, _w, h);
	}

	// horizontal growth
	pub fn text_h(&mut self, attrs: TextAttrs, cross: usize) -> UsedLength {
		let dbl_pad = attrs.pad * 2;
		let section = self.section(attrs.txt, (cross - dbl_pad) as f64);
		let bounds = self.bounds(&section);
		let px = (bounds.2 - bounds.0) as usize + dbl_pad;
		UsedLength {
			inner: None,
			grow: attrs.grow,
			px,
		}
	}
	
	// vertical growth
	pub fn text_v(&mut self, attrs: TextAttrs, cross: usize) -> UsedLength {
		let dbl_pad = attrs.pad * 2;
		let section = self.section(attrs.txt, (cross - dbl_pad) as f64);
		let bounds = self.bounds(&section);
		let px = (bounds.3 - bounds.1) as usize + dbl_pad;
		crate::console_log(&format!("{}", cross));
		UsedLength {
			inner: None,
			grow: attrs.grow,
			px,
		}
	}
	
	pub fn text_queue(&mut self, attrs: TextAttrs, x: usize, y: usize, _w: usize, h: usize) {
		let h_pad = h - 2 * attrs.pad;
		let x_pad = x + attrs.pad;
		let y_pad = y + attrs.pad;
		let position = (x_pad as f32, y_pad as f32);
		let mut section = self.section(attrs.txt, h_pad as f64);
		section.text[0].extra.color[0] = 0f32;
		section.text[0].extra.color[1] = 0f32;
		self.brush.queue(section.with_screen_position(position));
		crate::element_render(0, x, y, _w, h);
	}
}
