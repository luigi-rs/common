mod text;
use text::TextContext;
pub use text::LabelAttrs;
pub use text::TextAttrs;

#[derive(Debug)]
pub struct Context {
	pub text: TextContext,
}

impl Context {
	pub fn new() -> Self {
		Context {
			text: TextContext::new(),
		}
	}
}

#[derive(Debug)]
pub struct UsedLength {
	pub inner: Option<(Vec<(usize, UsedLength)>, bool)>,
	pub grow: usize,
	pub px: usize,
}

pub const GROW_NO: usize = 0;
pub const GROW_BEFORE: usize = 1;
pub const GROW_AROUND: usize = 2;
pub const GROW_AFTER: usize = 3;

pub struct Font {
	pub ch: usize,
}

pub struct Style {
	pub font: Font,
}

#[allow(non_upper_case_globals)]
pub const style: Style = Style {
	font: Font {
		ch: 24
	},
};

pub fn get_c<'a>(children: &'a Vec<(usize, UsedLength)>, i: usize) -> &'a UsedLength {
	for c in children {
		if c.0 == i {
			return &c.1;
		}
	}
	unreachable!()
}

pub fn len_acc(acc: usize, used_len: &(usize, UsedLength)) -> usize {
	acc + used_len.1.px
}

pub fn compute_cross(children: &Vec<(usize, UsedLength)>) -> usize {
	let mut max = 0;
	for used_len in children {
		if let Some((children, same_axis)) = &used_len.1.inner {
			let v = match same_axis {
				true => compute_cross(&children),
				false => children.iter().fold(0, len_acc),
			};
			if v > max {
				max = v;
			}
		}
	}
	max
}

pub fn element_h(_: usize, cross: usize) -> UsedLength {
	UsedLength {
		inner: None,
		grow: GROW_NO,
		px: cross * 3,
	}
}

pub fn element_v(_: usize, cross: usize) -> UsedLength {
	UsedLength {
		inner: None,
		grow: GROW_NO,
		px: cross / 4,
	}
}

extern "C" {
	fn raw_rect(x: usize, y: usize, w: usize, h: usize);
	fn raw_log(s: *const u8, l: usize);
}

pub fn element_render(_: usize, x: usize, y: usize, w: usize, h: usize) {
	unsafe {
		raw_rect(x, y, w, h);
	}
}

pub fn console_log(msg: &str) {
	unsafe {
		raw_log(&msg.as_bytes()[0], msg.len());
	}
}
